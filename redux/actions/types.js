import qs from 'query-string'

import { SENATICS_URL, RENEW_INTERVAL, TOKEN_EXPIRATION } from '../../constants';

export const REQUEST_TOKEN = "REQUEST_TOKEN";
export const requestToken = (token, secret) => {
  return {
    type: REQUEST_TOKEN
  };
};

export const RECEIVE_TOKEN = "RECEIVE_TOKEN"
export const receiveToken = (accessToken) => {
  return {
    type: RECEIVE_TOKEN,
    token : {
      accessToken,
      receivedAt: Date.now()
    }
  };
};


const ACCESS_TOKEN_URL = `${SENATICS_URL}/auth/token`
export const fetchToken = (token, secret) => {
  return (dispatch) => {
    dispatch(requestToken());

    const payload = {
      "clientSecret" : secret
    }

    const options = {
        method: "POST",
        body: JSON.stringify(payload),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Basic ${token}`
        },
      }

    const internal = () =>  fetch(ACCESS_TOKEN_URL, options)
      .then(
        response => response.json(),
        error =>  console.error('Error en la llamada', error) )
      .then( json => {
        setTimeout(function () {
          dispatch(fetchToken(token, secret));
        }, RENEW_INTERVAL);

        return dispatch( receiveToken( json.accessToken ) )
      })
    return internal()
  };
}

export const FETCH_TOKEN_ERROR = "FETCH_TOKEN_ERROR";
// TODO: procesar el caso de error;0

export const REQUEST_SOLICITUDES = "REQUEST_SOLICITUDES";
export const requestSolicitudes = (token, secret) => {
  return {
    type: REQUEST_SOLICITUDES
  };
};

export const RECEIVE_SOLICITUDES = "RECEIVE_SOLICITUDES"
export const receiveSolicitudes = (solicitudes) => {
  return {
    type: RECEIVE_SOLICITUDES,
    solicitudes: solicitudes.results,
  };
};

export const fetchSolicitudes = (token) => {

  return (dispatch) => {
    dispatch(requestSolicitudes());
    const options = {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }

    const params = {
      page: 1,
    }

    const url = `${SENATICS_URL}/solicitudes?${qs.stringify(params)}`;

    const internal = () =>  fetch(url, options)
      .then(
        response => response.json(),
        error =>  console.error('Error en la llamada', error) )
      .then( json => dispatch( receiveSolicitudes( json ) ) )
    return internal()
  }
}
