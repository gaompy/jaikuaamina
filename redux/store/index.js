import { createStore,  applyMiddleware } from 'redux';
import reducers from '../reducers';
import thunkMiddleware from 'redux-thunk'
import { fetchToken } from '../actions/types';

const store = createStore(reducers,
   applyMiddleware(thunkMiddleware) );

export default store;
