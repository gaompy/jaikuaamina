 import { REQUEST_TOKEN, RECEIVE_TOKEN } from '../actions/types'

const initial = {
  authenticated: false,
  loading: false,
  token: "",
  tokenTime: null,
}

export default ( state = initial, action ) => {
  switch(action.type){
    case REQUEST_TOKEN:
      return Object.assign({}, state, {
          loading: true,
      })
    case RECEIVE_TOKEN:
      return Object.assign({}, state, {
        authenticated: true,
        loading: false,
        token: action.token.accessToken,
        tokenTime: action.token.receivedAt,
      })
    default:
      return state;
  }
}
