import { combineReducers } from 'redux';
import solicitudes from './solicitudes';
import autenticacion from './autenticacion';

export default combineReducers({
  solicitudes,
  autenticacion
});
