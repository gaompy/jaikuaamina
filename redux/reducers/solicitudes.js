import { REQUEST_SOLICITUDES, RECEIVE_SOLICITUDES } from '../actions/types'
import { merge, mergeAll, filter, contains } from 'ramda'
import { ESTADOS_SOLICITUDES } from '../../constants'

const solicitudes = {
  data: {},
  own: [],
  current: null,
  loading: false,
  pendientes: [],
  finalizadas: []
}

const esPendiente = (solicitud) => {
  return contains(
    solicitud.estado.nombre,
    ESTADOS_SOLICITUDES.pendientes)
}

const esFinal = (solicitud) => {
  return contains(
    solicitud.estado.nombre,
    ESTADOS_SOLICITUDES.finalizados)
}

const receive = ( data ) => {
  return {
    data,
    pendientes: filter(esPendiente, data),
    finalizadas: filter(esFinal, data)
  }
}

export default (state, action) => {
  switch (action.type) {
    case REQUEST_SOLICITUDES:
      return merge(state, { loading: true })
    case RECEIVE_SOLICITUDES:
      return mergeAll( [
        state,
        { loading: false},
        receive( action.solicitudes ) ] )
    default:
      return state || solicitudes;
  }
} ;
