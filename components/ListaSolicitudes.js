import React, { Component } from 'react';
import { View, Dimensions, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { fetchSolicitudes } from '../redux/actions/types'

const css = StyleSheet.create({
  cabecera: {
    height: 50,
    backgroundColor: '#DDDD',
    alignItems: 'center',
    justifyContent: 'center'
  }, container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
  }
});


class ListaSolicitudes extends Component {
  componentDidMount () {
    this.props.fetchSolicitudes(this.props.token)
  }

  render() {
    return (
      <View style={css.container}>
        <View style={css.cabecera} >
            <Text> PENDIENTES </Text>
        </View>

          <View style={css.cabecera}>
            <Text> FINALIZADAS </Text>
          </View>

      </View>);
  }
}

const mapStateToProps = (state) =>{
  return {
    token: state.autenticacion.token,
    solicitudes: state.solicitudes.data
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSolicitudes: (token) => dispatch(fetchSolicitudes(token))
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (ListaSolicitudes);
