import { Constants } from 'expo';

export const STATUS_BAR_HEIGHT = 0 ; //Constants.statusBarHeight;

export const REQUEST_TOKEN = "68536364-26df-4720-9070-0efe5daecba4";
export const CLIENT_SECRET = "8c4202440b4af91a246d79fee6ee79ca25ac7525e23952f64b7995b5f64d5928";
export const SENATICS_URL = "http://192.168.43.82:2128/api";
export const RENEW_INTERVAL = 1 * 1000 * 60;
export const TOKEN_EXPIRATION = 15 * 1000 * 60;

export const ESTADOS_SOLICITUDES = {
  finalizados: [
    "RECHAZADO",
    "NO_RESPONDIDO",
    "RESPONDIDO",
    "RESPONDIDO_FUERA_PLAZO",  ],
  pendientes: [
    "PROCESO_JUDICIAL",
    "RECONSIDERACION_ATENDIDA",
    "INICIADO",
    "DERIVADO",
    "RECONSIDERADO",
    "EXTERNO",
    "INEXISTENTE",
    "REVOCADO",
    "EN_RECONSIDERACION",
    "RECONSIDERACION_NO_ATENDIDA",
    "DESACTUALIZADA",
    "RECONSIDERACION_ATENDIDA_FUERA_DE_PLAZO",
  ]
}
