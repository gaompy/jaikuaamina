import React, { Component } from 'react';
import { View, Platform, Text, ActivityIndicator, StyleSheet } from 'react-native';
import { STATUS_BAR_HEIGHT, CLIENT_SECRET,  REQUEST_TOKEN } from '../constants';
import { connect } from 'react-redux';
import { fetchToken } from '../redux/actions/types';
import ListaSolicitudes from '../components/ListaSolicitudes'
import { Icon } from 'react-native-elements'

// AIP Movil
const css = StyleSheet.create({
  activity: {
    justifyContent: 'center'
  },
  pane: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  footer: {
    height: 50,
    backgroundColor: '#2196F3',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
  },
});

class MainScreen extends Component {
  static navigationOptions = () => ({
    title: 'Mis solicitudes',
    headerStyle: {
      height: Platform.OS === 'android' ? 54 + STATUS_BAR_HEIGHT : 54,
      backgroundColor: '#2196F3'
    },
    headerTitleStyle: {
      marginTop: Platform.OS === 'android' ? STATUS_BAR_HEIGHT : 0,
      color: 'white'
    },
  });

  componentDidMount() {
    this.props.fetchToken(REQUEST_TOKEN, CLIENT_SECRET)
  }

  render() {
    const lista = <ListaSolicitudes />
    const spinner = <ActivityIndicator size="large" color="#2196F3" style={css.activity} />

    return (
      <View style={css.pane}>
        <View style={css.pane}>
          {this.props.auth.authenticated ? lista : spinner}
        </View>
        <View style={css.footer}>
          <Icon name='add-circle' color='#FFF' />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.autenticacion
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchToken: ( token, secret ) => dispatch( fetchToken(token, secret) )
  }

};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
